import {Injectable} from '@angular/core';
import {User} from '../../models/user';

@Injectable()
export class UserService {

  register(user: User) {
    console.log("about to register " + user.email);
  }

  login(user: User) {
    console.log("logging in " + user.email);
  }
}
