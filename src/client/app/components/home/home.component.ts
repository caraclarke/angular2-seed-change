// libs
import {Store} from '@ngrx/store';

// import PassitSDK from 'passit-sdk-js';
// console.log(PassitSDK);

// import PassitSDK from 'passit-sdk-js';
// let sdk = new PassitSDK();
// console.log(sdk);

// app
import {FormComponent, RouterExtensions} from '../../frameworks/core/index';
import {NameListService} from '../../frameworks/app/index';

// user
import {User} from '../../models/user';
import {UserService} from './user.service';

@FormComponent({
  moduleId: module.id,
  providers: [UserService],
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})
export class HomeComponent {
  user: User;
  isLoggingIn = true;

  // TODO: clean up constructor
  constructor(
    private store: Store<any>,
    public nameListService: NameListService,
    public routerext: RouterExtensions,
    private _userService: UserService) {
    this.user = new User();

  }

  toggleDisplay() {
    this.isLoggingIn = !this.isLoggingIn;
    console.log(this.isLoggingIn);
  }

  submit() {
    if (this.isLoggingIn) {
      this.login();
    } else {
      this.signUp();
    }
  }

  login() {
    this._userService.login(this.user);
    // TODO: .subscribe?
  }

  signUp() {
    this._userService.register(this.user);
  }

}
